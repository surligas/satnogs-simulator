# satnogs-simulator: A simulation tool for the SatNOGS network

The satnogs-simulator is a CLI based tool, that simulates the SatNOGS network with
orbiting satellites.

The simulation parameters are specified through a YAML file.
Such a file can be found at the `contrib` directory.

Users can specify:
* The resolution of the simulation in miliseconds
* The start and stop date
* The tool makes use of a thread pool. Its size is also configurable
* Satellite information can be retrieved from the SatNOGS-DB. Custom
satellites are also supported by specifying the name and the TLE of the custom entry
* Ground station information can be retrived automatically from the SatNOGS network.
Again, custom ground stations can be defined too
* The simulation supports a basic transmission scheme for each satellite.
Messages are randommly "transmitted". Those messages are delivered to stations that
have a line of sight with the corresponding satellite. The line of site depends on the
ground station configured minimum horizon parameter.
* A set of events that can occur on the satellite while orbiting, are also supported.
Currently random reboots and reboots caused by the South-Atlantic-Anomaly are implemented

## Output Data
The simulation produces two different CSV files, one containing the reception log and one the events log as they occur on each spacecraft.

The first specified by the `reception-log` parameter of the `YAML` configuration file, logs the received frames from all the available ground stations.
This logs contains telemetry from the satellite, plus some extra telemetry from the ground station when this particular frame was received (e.g ground station - satellite relative position).

| Date                        | Satellite      | Satellite Azimuth                         | Satellite Elevation                         | Satellite Range                           | Attitude Q[0]                                                                                                                                                           | Attitude Q[1]                                                                                                                                                           | Attitude Q[2]                                                                                                                                                           | Attitude Q[3]                                                                                                                                                           | Battery Capacity (mWh)  | Solar Panel Total (mW)          | Solar Panel X+ (mW)                | Solar Panel X- (mW)                | Solar Panel Y+ (mW)                | Solar Panel Y- (mW)                | Solar Panel Z+ (mW)                | Solar Panel Z- (mW)                | Satellite Uptime (ms)             | Satellite Resets        | Ground Station                          |
| --------------------------- | -------------- | ----------------------------------------- | ------------------------------------------- | ----------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- | ------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | --------------------------------- | ----------------------- | --------------------------------------- |
| Frame reception time in UTC | Satellite name | Satellite azimuth from the GS perspective | Satellite elevation from the GS perspective | Distance between the GS and the satellite | Q[0] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Q[1] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Q[2] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Q[3] quaternion indicating the orientation of the body with respect to earth. Refer to “Fundamentals of Spacecraft Attitude Determination and Control” for more details | Battery capacity in mWh | Power from all available panels | Power from the X+ axis solar panel | Power from the X- axis solar panel | Power from the Y+ axis solar panel | Power from the Y- axis solar panel | Power from the Z+ axis solar panel | Power from the Z- axis solar panel | Satellite uptime since last reset | Satellite reset counter | Ground station that received this frame |

The events log specified by the `events-log` parameter of the `YAML` configuration file, logs the various events and anomalies at the time they occur at each satellite.
The purpose of this log is mainly for cross-validation.
The format of the file is the following:

| Date                               | Satellite                                      | Satellite Longitude                             | Satellite Latitude                             | Satellite Altitude                             | Event                 |
| ---------------------------------- | ---------------------------------------------- | ----------------------------------------------- | ---------------------------------------------- | ---------------------------------------------- | --------------------- |
| The time in UTC the event occurred | The satellite name on which the event occurred | The satellite longitude when the ebent occurred | The satellite latitude when the ebent occurred | The satellite altitude when the ebent occurred | The type of the event |


## Requirements

To build the `satnogs-simulator` the following software packages should be available:

* CMake (>= 3.20)
* C++17
* GNU Make
* spdlog
* cpprest
* boost-random
* yaml-cpp
* eigen3

To install the dependencies in Debian based distros, you can use the following commands:
```bash
sudo apt-get install -qy --no-install-recommends git ca-certificates cmake cmake-extras make gcc gcc-multilib g++-multilib  libcpprest-dev libboost-system-dev libboost-random-dev libspdlog-dev libeigen3-dev
```

To build the tool use the commands below:
```bash
cd satnogs-simulator
mkdir build
cd build
cmake ..
make
```

## Performance
The architecture of the simulation tool has been optimized for perfomance and speed
trying to reduce the critical sections between trheads.

> [!WARNING]
> There is a known issue with the REST API of SatNOGS, being to aggressive on the throttling.
> This will delay considerably the fetching of satellites and especially the ground stations.


## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2024 [Libre Space Foundation](https://libre.space).

