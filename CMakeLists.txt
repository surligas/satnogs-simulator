cmake_minimum_required(VERSION 3.20)
project(satnogs-simulator CXX C)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)

# ##############################################################################
# CMake custom modules
# ##############################################################################
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake)

# uninstall target
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake/cmake_uninstall.cmake" IMMEDIATE @ONLY)

add_custom_target(
  uninstall COMMAND ${CMAKE_COMMAND} -P
                    ${CMAKE_CURRENT_BINARY_DIR}/cmake/cmake_uninstall.cmake)

include(cmake/CPM.cmake)
cpmaddpackage("gh:dnwrnr/sgp4@1.0")

# Mandatory requirements
find_package(Threads REQUIRED)
find_package(yaml-cpp REQUIRED)
find_package(spdlog REQUIRED)
find_package(sgp4 REQUIRED)
find_package(Boost COMPONENTS random)
find_package(cpprestsdk REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(Eigen3 REQUIRED)

# ##############################################################################
# Setup the include and linker paths
# ##############################################################################
include_directories(${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR})

# ##############################################################################
# Installation details
# ##############################################################################
include(CPack)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
add_executable(
  satnogs-simulator
  src/anomalies.cpp
  src/attitude.cpp
  src/battery.cpp
  src/event.cpp
  src/gs.cpp
  src/main.cpp
  src/msg.cpp
  src/power.cpp
  src/power.cpp
  src/power_spike.cpp
  src/sat_link.cpp
  src/satellite.cpp
  src/simulation.cpp
  src/ssa.cpp
  src/telemetry.cpp
  src/tumbling.cpp
  src/wdg_reset.cpp)

target_include_directories(satnogs-simulator
                           PUBLIC $<BUILD_INTERFACE:${sgp4_SOURCE_DIR}>)

target_link_libraries(
  satnogs-simulator
  PUBLIC m
         ${CMAKE_THREAD_LIBS_INIT}
         yaml-cpp::yaml-cpp
         spdlog::spdlog
         sgp4
         cpprestsdk::cpprest
         OpenSSL::Crypto
         Boost::random
  PRIVATE sgp4)
