/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "battery.hpp"
#include <algorithm>

/**
 * @brief Construct a new battery::battery object
 *
 * @param max_capacity maximum capacity in mWh
 * @param min_safe_capacity minimum safety capacity in mWh
 * @param capacity initial capacity in mWh
 */
battery::battery(double max_capacity, double min_safe_capacity, double capacity)
    : m_max_capacity(max_capacity),
      m_min_safe_capacity(min_safe_capacity),
      m_capacity(capacity)
{
}

double
battery::max_capacity() const
{
  return m_max_capacity;
}

double
battery::min_safe_capacity() const
{
  return m_min_safe_capacity;
}

double
battery::capacity() const
{
  return m_capacity;
}

void
battery::drain(double mWh)
{
  m_capacity = std::max(0.0, m_capacity - mWh);
}

void
battery::charge(double mWh)
{
  m_capacity = std::min(m_max_capacity, m_capacity + mWh);
}
