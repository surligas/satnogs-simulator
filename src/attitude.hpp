/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include "eigen3/Eigen/Dense"
#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <iostream>
#include <libsgp4/Vector.h>

class attitude
{
public:
  attitude();
  attitude(Eigen::Vector4d q);

  void
  set_q(const Eigen::Vector4d q);
  Eigen::Vector4d
  get_q() const;

  void
  set_tumbling(bool is_tumbling);
  bool
  get_tumbling();

  static Eigen::Vector4d
  DCM_to_q(const Eigen::Matrix3d &DCM);

  static Eigen::Matrix3d
  q_to_DCM(const Eigen::Vector4d &q);

  static Eigen::Matrix3d
  calc_L_frame(const Vector &pos, const Vector &vel);

private:
  /**
   * Quaternion from the body (B) to the ECI (N) frame. Scalar-first convention.
   */
  Eigen::Vector4d m_qbn;

  /**
   * true if the satellite is in a tumbling state
   */
  bool m_tumbling;
};
