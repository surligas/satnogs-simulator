/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "gs.hpp"
#include "sat_link.hpp"

gs::gs(const std::string &name, double lat, double lng, double elev,
       double min_horizon)
    : m_name(name),
      m_lat(lat),
      m_lon(lng),
      m_elev(elev),
      m_min_horizon(min_horizon),
      m_max_aos(0),
      m_downlink_cnt(0)
{
}

double
gs::latitude() const
{
  return m_lat;
}

double
gs::longitude() const
{
  return m_lon;
}

double
gs::elevation() const
{
  return m_elev;
}

double
gs::min_horizon() const
{
  return m_min_horizon;
}

void
gs::update(const DateTime &t, std::shared_ptr<spdlog::logger> logger)
{
  /* Update all registered links */
  auto observer = Observer(m_lat, m_lon, m_elev);
  for (auto i : m_links) {
    auto x = i->get_downlink();
    while (x) {
      auto q          = x->tlm().att().get_q();
      auto pwr        = x->tlm().pwr();
      auto pwr_panels = pwr.get_power_in_per_side();
      auto pos        = observer.GetLookAngle(x->position());
      logger->info(
          x->timestamp().ToString() + ", " + i->sat_name() + ", " +
          std::to_string(Util::RadiansToDegrees(pos.azimuth)) + ", " +
          std::to_string(Util::RadiansToDegrees(pos.elevation)) + ", " +
          std::to_string(Util::RadiansToDegrees(pos.range)) + ", " +
          std::to_string(q[0]) + ", " + std::to_string(q[1]) + ", " +
          std::to_string(q[2]) + ", " + std::to_string(q[3]) + ", " +
          std::to_string(pwr.bat().capacity()) + ", " +
          std::to_string(pwr.get_total_power_in()) + ", " +
          std::to_string(pwr_panels[0]) + ", " + std::to_string(pwr_panels[1]) +
          ", " + std::to_string(pwr_panels[2]) + ", " +
          std::to_string(pwr_panels[3]) + ", " + std::to_string(pwr_panels[4]) +
          ", " + std::to_string(pwr_panels[5]) + ", " +
          std::to_string(x->tlm().uptime_ms()) + ", " +
          std::to_string(x->tlm().reset_cnt()) + ", " + m_name);
      m_downlink_cnt++;
      x = i->get_downlink();
    }
  }

  /* Update statistics */
  size_t cnt = std::count_if(m_links.cbegin(), m_links.cend(),
                             [](const sat_link::sptr l) { return l->aos(); });
  m_max_aos  = std::max(m_max_aos, cnt);
}

void
gs::register_link(sat_link::sptr l)
{
  m_links.push_back(l);
}

size_t
gs::max_aos() const
{
  return m_max_aos;
}

void
gs::print_results() const
{
  std::cout << "Name                                    :  " << m_name
            << std::endl;
  std::cout << "Max satellites at the field of view     :  " << max_aos()
            << std::endl;
  std::cout << "Frames received                         :  " << m_downlink_cnt
            << std::endl;
}
