/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "satellite.hpp"
#include "sat_link.hpp"

satellite::satellite(size_t id, const std::string &name, const std::string &tle,
                     anomalies &a, const power &pwr, double tlm_prob,
                     std::shared_ptr<spdlog::logger> logger)
    : m_id(id),
      m_name(name),
      m_tle(tle),
      m_first_update(true),
      m_sgp4({name, tle.substr(0, 69), tle.substr(69 + 1, 69)}),
      m_eci(DateTime(), 0, 0, 0),
      m_anomalies(a),
      m_rd(),
      m_gen(m_rd()),
      m_reset_dist(a.reset_prob()),
      m_ssa_dist(a.get_ssa().prob()),
      m_tlm_dist(tlm_prob),
      m_att(),
      m_pwr(pwr),
      m_normal_consumption(m_pwr.consumption()),
      m_tlm(m_att, m_pwr),
      m_logger(logger)
{
}

const std::string &
satellite::tle() const
{
  return m_tle;
}

std::string
satellite::tle(size_t line) const
{
  if (line > 2 || line < 1) {
    throw std::invalid_argument("Wrong number of line. [1,2] allowed");
  }
  if (line == 1) {
    return m_tle.substr(0, 69);
  }
  return m_tle.substr(69 + 1, 69);
}

size_t
satellite::id() const
{
  return m_id;
}

const std::string &
satellite::name() const
{
  return m_name;
}

void
satellite::update(const DateTime &t)
{
  if (m_first_update) {
    m_timestamp        = t;
    m_persec_timestamp = t;
    m_wdg_timestamp    = t;
    m_first_update     = false;
    m_eci              = m_sgp4.FindPosition(t);
  } else {

    /* Model the anomalies once per second */
    auto secs = (t - m_persec_timestamp).TotalSeconds();
    while (secs > 0) {
      m_tlm.update_uptime(1000000);
      m_timestamp        = m_timestamp.AddSeconds(1);
      m_persec_timestamp = m_persec_timestamp.AddSeconds(1);
      m_eci              = m_sgp4.FindPosition(m_timestamp);

      update_attitude();
      update_power();

      /* Model anomalies from SSA */
      if (m_anomalies.get_ssa().affected(
              {Util::RadiansToDegrees(m_eci.ToGeodetic().latitude),
               Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)},
              m_eci.ToGeodetic().altitude * 1000)) {
        if (m_ssa_dist(m_gen)) {
          m_tlm.reset();
          log_ssa_reset();
        }
      }

      /* Model resets */
      if (m_reset_dist(m_gen)) {
        m_tlm.reset();
        log_reset();
      }

      /* Model all other events */
      const auto &events           = m_anomalies.events(id());
      bool        tumbling_active  = false;
      bool        pwr_spike_active = false;
      for (const auto &i : events) {
        if (auto tumb = dynamic_cast<tumbling *>(i.get())) {
          if (tumb->active(m_timestamp)) {
            m_att.set_tumbling(true);
            log_tumbling();
            tumbling_active = true;
          }
        } else if (auto spike = dynamic_cast<power_spike *>(i.get())) {
          if (spike->active(m_timestamp)) {
            m_pwr.set_consumption(spike->consumption());
            log_power_spike();
            pwr_spike_active = true;
          }
        } else if (auto wdg = dynamic_cast<wdg_reset *>(i.get())) {
          if (wdg->active(m_timestamp) &&
              (m_timestamp - m_wdg_timestamp).TotalSeconds() > wdg->period()) {
            m_tlm.reset();
            log_wdg_reset();
            m_wdg_timestamp = m_timestamp;
          }
        }
      }

      if (!tumbling_active) {
        m_att.set_tumbling(false);
      }

      if (!pwr_spike_active) {
        m_pwr.set_consumption(m_normal_consumption);
      }

      /* Model telemetry transmissions. Messages will be delievered only is there is AOS */
      if (m_tlm_dist(m_gen)) {
        for (auto i : m_links) {
          i->downlink(msg::sptr(new msg(m_timestamp, m_eci, m_tlm)));
        }
      }
      secs--;
    }
    m_tlm.update_uptime((t - m_timestamp).TotalMicroseconds());
    m_timestamp = t;
  }
}

Eci
satellite::position() const
{
  return m_eci;
}

const telemetry &
satellite::tlm() const
{
  return m_tlm;
}

void
satellite::update_attitude()
{
  if (m_att.get_tumbling()) {
    std::uniform_real_distribution<double> distribution(-1.0, 1.0);
    Eigen::Vector4d                        random_q;
    for (int i = 0; i < 4; ++i) {
      random_q[i] = distribution(m_gen);
    }
    random_q /= random_q.norm();
    m_att.set_q(random_q);
  } else {

    Eigen::Matrix3d Rln =
        attitude::calc_L_frame(m_eci.Position(), m_eci.Velocity()).transpose();

    // Update m_qbn and m_Rbn
    Eigen::Vector4d qln_scalar_last = attitude::DCM_to_q(Rln);
    Eigen::Vector4d qln_scalar_first;
    qln_scalar_first << qln_scalar_last[3], qln_scalar_last.segment(0, 3);
    m_att.set_q(qln_scalar_first);
  }
}

void
satellite::update_power()
{
  m_pwr.calc_power_in(m_eci.Position(), m_att.get_q(), m_timestamp);
  m_pwr.update_battery_capacity(m_timestamp);
}

void
satellite::log_reset()
{
  m_logger->info(
      m_timestamp.ToString() + ", " + name() + ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)) +
      ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().latitude)) +
      ", " + std::to_string(m_eci.ToGeodetic().altitude) + ", RST_NORMAL");
}

void
satellite::log_ssa_reset()
{
  m_logger->info(
      m_timestamp.ToString() + ", " + name() + ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)) +
      ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().latitude)) +
      ", " + std::to_string(m_eci.ToGeodetic().altitude) + ", RST_SSA");
}

void
satellite::log_tumbling()
{
  m_logger->info(
      m_timestamp.ToString() + ", " + name() + ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)) +
      ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().latitude)) +
      ", " + std::to_string(m_eci.ToGeodetic().altitude) + ", TUMBLING");
}

void
satellite::log_power_spike()
{
  m_logger->info(
      m_timestamp.ToString() + ", " + name() + ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)) +
      ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().latitude)) +
      ", " + std::to_string(m_eci.ToGeodetic().altitude) + ", POWER_SPIKE");
}

void
satellite::log_wdg_reset()
{
  m_logger->info(
      m_timestamp.ToString() + ", " + name() + ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().longitude)) +
      ", " +
      std::to_string(Util::RadiansToDegrees(m_eci.ToGeodetic().latitude)) +
      ", " + std::to_string(m_eci.ToGeodetic().altitude) + ", RST_WDG");
}

void
satellite::register_link(sat_link::sptr l)
{
  m_links.push_back(l);
}
