/*
 *  satnogs-simulator: Satellite and Ground station network simulation tool
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include "anomalies.hpp"
#include "gs.hpp"
#include "sat_link.hpp"
#include "satellite.hpp"
#include <map>
#include <mutex>
#include <spdlog/spdlog.h>
#include <thread>
#include <vector>
#include <yaml-cpp/yaml.h>

class simulation
{
public:
  simulation(const YAML::Node &config);

  void
  start();

  void
  results();

  static DateTime
  parse_iso8601_UTC(const std::string &date);

private:
  const YAML::Node                 m_config;
  size_t                           m_max_threads;
  size_t                           m_resolution_ms;
  std::vector<gs>                  m_gs;
  std::map<std::string, satellite> m_sats;
  std::vector<sat_link::sptr>      m_links;
  DateTime                         m_start_time;
  DateTime                         m_stop_time;
  DateTime                         m_cur_time;
  anomalies                        m_anomalies;
  std::mutex                       m_mtx;
  std::shared_ptr<spdlog::logger>  m_event_logger;
  spdlog::file_event_handlers      m_recv_spdlog_handlers;
  spdlog::file_event_handlers      m_event_spdlog_handlers;

  void
  create_stations(const YAML::Node &config);

  void
  create_sats(const YAML::Node &config);

  void
  create_links();

  void
  print();

  void
  run(size_t idx, size_t n);
};
